# KVM Setup on iMX8 Hummingboard Pulse

There are two ways to run VM via KVM
* qemu (most direct)
* libvirt + qemu (for better VM managability)

## Method 1 - Using QEMU

### Create VM
First create disk image for the VM
```
dd if=/dev/zero of= debian10-arm64.img bs=1M count=8192
```
or
```
qemu-img create -f qcow2 debian10-arm64.img 8G
```

Download the ARM64 `netinst iso` from Debian here which is only 150MB and will download packages from Internet instead during installation. We use `debian-10.1.0-arm64-netinst.iso`.

### Launching VM
Launch the VM using the following config. Note the mounted cdrom for initial installation. This also setup a local SSH forwarding at port 5555 which we can then SSH into the VM from the imx8 host, i.e. `ssh debian@127.0.0.1 –p 5555`.

```
qemu-system-aarch64 -enable-kvm -m 2G -M virt -cpu host \
  -bios /usr/share/qemu-efi-aarch64/QEMU_EFI.fd \
  -drive file=debian-10.1.0-arm64-netinst.iso,id=cdrom,if=none,media=cdrom \
  -device virtio-scsi-device -device scsi-cd,drive=cdrom \
  -drive if=none,file=debian10-arm64.img,id=hd0\
  -device virtio-blk-device,drive=hd0\
  -device e1000,netdev=eth0 -netdev user,id=eth0,hostfwd=tcp:127.0.0.1:5555-:22 \
  -boot d -machine gic-version=3 -nographic
```

Note:
* `-machine gic-version=3` argument is needed for machines with GICv3 that don't support GICv2 guests. @ref: https://www.linaro.org/blog/whats-new-qemu-2-9/
* https://station.eciton.net/installing-debian-jessie-on-qemu-aarch64.html has a good explanation on the arguments.



## Method 2 – Use libvirtd to manage VM

### Setup Networking
Create Bridge network interface from existing eth0 that all VM can share
There are 3 options:
1.	Using virsh net-startUsing Linux bridge 
2.	Using bridge-utils
3.	Using Linux bridge

#### Option 1
By default, when `libvirtd` daemon is started, it will automatically run the `virbr0` interface. You may check the log in `systemctl status libvirtd`. This option cannot be use if the service fail to create the bridge.
```
virsh net-start default
virsh net-list --all
```

The `default` network profile can be used as starting point and then define in the network.
```
virsh net-dumpxml default > br0.xml
virsh net-define br0
virsh net-start br0
```

Note that this option require that the `nftables` kernel module is loaded, otherwise the following error will show when checking the `systemctl status libvirt`
```
... protocol not supported
```

 
#### Option 2
@ref: https://jamielinux.com/docs/libvirt-networking-handbook/bridged-network.html

Get the MAC address of the existing `eth0` interface, e.g. `da:ef:8c:66:56:69` for our Hummingboard Pulse.

```
ip address show dev eth0 | awk '$1=="link/ether" {print $2}'
da:ef:8c:66:56:69 
```
Configure the bridge using the `bridge-utils` package.
```
sudo apt install bridge-utils
```

Modify the `eth0` interface in `nano /etc/network/interfaces.d/eth0` with the following:
```
iface eth0 inet manual

auto br0
iface br0 inet static
    # Use the MAC address identified above.
    hwaddress ether da:ef:8c:66:56:69
    address 140.113.169.20
    netmask 255.255.255.0
    gateway 140.113.169.254

    bridge_ports eth0
    # If you want to turn on Spanning Tree Protocol, ask your hosting
    # provider first as it may conflict with their network.
    bridge_stp off
    # If STP is off, set to 0. If STP is on, set to 2 (or greater).
    bridge_fd 0

```

Restart `eth0`.
```
ip address flush eth0 scope global && ifup br0
```

Check if bridge is up and running.
```
brctl show br0
```

### Create VM
Using the bridge, create a VM using `virsh` tool. Notice the additional `–network bridge=br0` flag passed which is the network we defined earlier. Without this argument `libvirtd` will automatically use the `default` bridge, which might fail if you do not have `nftables` kernel module loaded.

The following will create a VM that uses 2 pCPU>vCPU.
```
virt-install --name=debian10-vm1 \
--cpuset=0-1 \
--memory=1024 \
--cdrom /root/qemu-disk-image/debian-10.1.0-arm64-netinst.iso \
--disk size=4 \
--os-variant=debian10 \
--network bridge=br0
```

Note: If you encounter `Permission denied` error when mounting the cdrom ISO, modify the `/etc/libvirt/qemu.conf` file and uncomment the `user` and `group` line.
@ref: https://github.com/jedi4ever/veewee/issues/996 

```
# The user for QEMU processes run by the system instance. It can be
# specified as a user name or as a user id. The qemu driver will try to
# parse this value first as a name and then, if the name doesn't exist,
# as a user id.
#
# Since a sequence of digits is a valid user name, a leading plus sign
# can be used to ensure that a user id will not be interpreted as a user
# name.
#
# Some examples of valid values are:
#
#       user = "qemu"   # A user named "qemu"
#       user = "+0"     # Super user (uid=0)
#       user = "100"    # A user named "100" or a user with uid=100
#
user = "root"

# The group for QEMU processes run by the system instance. It can be
# specified in a similar way to user.
group = "root"
...
```

Then restart libvirt service.
```
systemctl restart libvirt
```

# Appendix
**Console access to VM**
```
virsh console <vmname>
```

**List all VM**
```
virsh list --all
```

**Start VM**
```
virsh start <vmname>
```

**Stop VM**
```
virsh stop <vmname>
```